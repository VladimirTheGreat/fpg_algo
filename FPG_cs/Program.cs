﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;

namespace FPG_cs
{
    public class Program
    {
        private static FbConnection CON;
        private static FbTransaction TR;
        private static FbCommand fbcCommand;
        private static List<int[]> ListIniTransactions;
        public static FPG_Tree fpg;

        public static void Initialize()
        {
            fpg = new FPG_Tree();
            ListIniTransactions = new List<int[]>();
            try
            {
                string ConnectionString = "User ID=sysdba;Password=masterkey;" +
                "Database=localhost:C:\\Users\\4igur\\Documents\\Toyota_DBs\\ToyotaDB.fdb; " +
                "DataSource=localhost;Charset=WIN1251;";
                CON = new FbConnection(ConnectionString);
                CON.Open();
                TR = CON.BeginTransaction();
                fbcCommand = new FbCommand(@" SELECT /*DIST_LIST_CLUST_IDS*/ DIST_SECOND_LEVEL_LIST_CLUSTERS FROM LIST_FACTORY_REQUESTS", CON, TR);
                FbDataReader reader2 = fbcCommand.ExecuteReader();
                while (reader2.Read())
                {
                    string[] Distinct_List_Of_Delivered_Clusters_2_Factory_During_A_SingleDay = reader2[0].ToString().Split(',').ToArray();
                    int[] SingleDay = new int[Distinct_List_Of_Delivered_Clusters_2_Factory_During_A_SingleDay.Count()];
                    for (int i = SingleDay.Length - 1; i >= 0; i--)
                        SingleDay[i] = int.Parse(Distinct_List_Of_Delivered_Clusters_2_Factory_During_A_SingleDay[i]);
                    ListIniTransactions.Add( SingleDay );
                }
                TR.Commit();
                reader2.Close();
                CON.Close();
                fbcCommand.Dispose();


                fpg.AddTransactionsDB(ListIniTransactions);
                fpg.BuildTree();
            }
            catch (Exception x)
            {
                Console.WriteLine("\nShet happened in Initialize()\n");
            }
        }
    public List<FPG_Tree.Itemset> CalculateSequence(int[] ContIds)
    {
      return fpg.CalculateSequence(ContIds);
    }

    static void Main(string[] args)
        {
            Initialize();
            while (true)
            {
                int support;
                int confidence;
                bool filterByLift;
                Console.Clear();
                Console.WriteLine("\n ===== АССОЦИАТИВНЫЙ АНАЛИЗ АЛГОРИТМОМ FPG ===== \n");
                Console.WriteLine(" Дерево построено, всего {0} транзакций, {1} уникальных элементов в них..\n", fpg.GetNumberTransactions(), fpg.GetNumberOfElements());
                Console.WriteLine(" == Введите поддержку ассоциативного правила (% от всех транзакций, в которых были и анцедент и консеквент)");
                support = int.Parse(Console.ReadLine());
                Console.WriteLine(" == Введите уверенность ассоциативного правила (отношение количества транзакций, в которых были и анцедент и консеквент, к общему количеству, в которых был анцедент):");
                confidence = int.Parse(Console.ReadLine());
                Console.WriteLine(" == Отфильтровать правила от отрицательной корреляции?");
                if (Console.ReadKey().KeyChar == '1')
                    filterByLift = true;
                else
                    filterByLift = false;
                try
                {
                    fpg.PerformFPGAnalysis(support, confidence, filterByLift);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }

                Console.WriteLine("\n == Анализ выполнен...");
                Console.WriteLine();
                Console.WriteLine(" Сохранить/продолжить/выйти (1/2/3?)\n");
                char ch = Console.ReadKey().KeyChar;
                switch (ch)
                {
                    case '3':
                        return;
                    case '1':
                        SaveAnalytics();
                        Console.WriteLine("\n Повторить/выйти (1/2?)\n");
                        if (Console.ReadKey().KeyChar == '2')
                            return;
                    break;
                    case '2':
                        continue;
                }
            }
        }
        private static void SaveAnalytics()
        {
            try
            {
                string ConnectionString = "User ID=sysdba;Password=masterkey;" +
                "Database=localhost:C:\\Users\\4igur\\Documents\\Toyota_DBs\\ToyotaDB.fdb; " +
                "DataSource=localhost;Charset=WIN1251;";
                CON = new FbConnection(ConnectionString);
                CON.Open();
                TR = CON.BeginTransaction();
                fbcCommand = new FbCommand(@" insert into LIST_PERFORMED_FPG_ANALYSES ( CONFIDENCE_THRESHOLD, SUPPORT_THRESHOLD, ID_LIST_CLUSTERIZATIONS, IS_LIFT_FILTERED )values (
                                              @CONFIDENCE_THRESHOLD
                                            , @SUPPORT_THRESHOLD
                                            , 78
                                            , @IS_LIFT_FILTERED) RETURNING ID;", CON, TR);
                fbcCommand.Parameters.Add("@CONFIDENCE_THRESHOLD", FbDbType.Double).Value = fpg.Conf;
                fbcCommand.Parameters.Add("@SUPPORT_THRESHOLD", FbDbType.Double).Value = fpg.Support;
                fbcCommand.Parameters.Add("@IS_LIFT_FILTERED", FbDbType.SmallInt).Value = 1;

                fbcCommand.Parameters[0].Value = fpg.Conf;
                fbcCommand.Parameters[1].Value = fpg.Support;
                fbcCommand.Parameters[2].Value = 1;

                //fbcCommand.ExecuteNonQuery();
                TR.Commit();
                
                var id_list_analyse = int.Parse(fbcCommand.ExecuteScalar().ToString());
                fbcCommand.Dispose();
                TR = CON.BeginTransaction();
                foreach (var z in FPG_Tree.Itemsets)
                {
                    fbcCommand = new FbCommand(@" insert into LIST_FREQUENT_ITEMSETS 
                        ( ID_LIST_PERFORMED_FPG_ANALYSES
                        , ANCEDENT_CLID
                        , CONSEQUENT_CLIDS
                        , CONSEQUENCE_OCCURENCE
                        , LIFT
                        , CONFIDENCE
                        , SUPPORT
                        , SUPPORT_STRING
                        , Leverege
                        , CONSEQUENCE_RARITY
                        ) VALUES
                        (  @ID_LIST_PERFORMED_FPG_ANALYSES
                        , @ANCEDENT_CLID
                        , @CONSEQUENT_CLIDS
                        , @CONSEQUENCE_OCCURENCE
                        , @LIFT
                        , @CONFIDENCE
                        , @SUPPORT
                        , @SUPPORT_STRING
                        , @Leverege
                        , @CONSEQUENCE_RARITY)", CON, TR);
                    fbcCommand.Parameters.Add("@ID_LIST_PERFORMED_FPG_ANALYSES", FbDbType.BigInt, System.Runtime.InteropServices.Marshal.SizeOf(id_list_analyse));
                    fbcCommand.Parameters.Add("@ANCEDENT_CLID", FbDbType.Integer, System.Runtime.InteropServices.Marshal.SizeOf(z.Ancedent));

                    fbcCommand.Parameters.Add("@CONSEQUENT_CLIDS", FbDbType.VarChar, z.StrConsequence.Length);
                    fbcCommand.Parameters.Add("@CONSEQUENCE_OCCURENCE", FbDbType.SmallInt, System.Runtime.InteropServices.Marshal.SizeOf(z.ConsequenceOccurence));
                    fbcCommand.Parameters.Add("@LIFT", FbDbType.Double, System.Runtime.InteropServices.Marshal.SizeOf(z.Lift));
                    fbcCommand.Parameters.Add("@CONFIDENCE", FbDbType.Integer, System.Runtime.InteropServices.Marshal.SizeOf(z.Confidence));
                    fbcCommand.Parameters.Add("@SUPPORT", FbDbType.Integer, System.Runtime.InteropServices.Marshal.SizeOf(z.Support));
                    fbcCommand.Parameters.Add("@SUPPORT_STRING", FbDbType.VarChar, z.SuppCount.Length);
                    fbcCommand.Parameters.Add("@Leverege", FbDbType.Double, System.Runtime.InteropServices.Marshal.SizeOf(z.Leverege));
                    fbcCommand.Parameters.Add("@CONSEQUENCE_RARITY", FbDbType.Double, System.Runtime.InteropServices.Marshal.SizeOf(z.ConsequenceRarity));

                    fbcCommand.Parameters[0].Value = id_list_analyse;
                    fbcCommand.Parameters[1].Value = z.Ancedent;
                    fbcCommand.Parameters[2].Value = z.StrConsequence;
                    fbcCommand.Parameters[3].Value = z.ConsequenceOccurence;
                    fbcCommand.Parameters[4].Value = z.Lift;
                    fbcCommand.Parameters[5].Value = z.Confidence;
                    fbcCommand.Parameters[6].Value = z.Support;
                    fbcCommand.Parameters[7].Value = z.SuppCount;
                    fbcCommand.Parameters[8].Value = z.Leverege;
                    fbcCommand.Parameters[9].Value = z.ConsequenceRarity;
                    fbcCommand.ExecuteNonQuery();
                }
                // Commit changes
                TR.Commit();
                // Free command resources in Firebird Server
                fbcCommand.Dispose();
                // Close connection
                CON.Close();
            }
            catch (Exception x)
            {
                Console.WriteLine("\nShet happened while saving results\n");
            }
        }
    }
}