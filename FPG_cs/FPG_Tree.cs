﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;

#region Helpers
public class Node
{
  internal int Cluster_id;
  internal int Occurences;
  internal Node Anchestor;            // чтоб от ноды удобно было путь к корню прокладывать
  internal List<Node> childs;
  internal Node(int Cl)
  {
    Cluster_id = Cl;
    Occurences = 1;
    childs = new List<Node>();
  }
}
public class Element
{
  internal int ClusterId;
  internal int TotOccs;
  internal Element(int n)
  {
    ClusterId = n;
    TotOccs = 1;
  }
  internal Element(int n, int s)
  {
    ClusterId = n;
    TotOccs = s;
  }
}
public class Path
{
  internal int Support;
  internal List<int> Prefix;
  internal int Suffix;
  internal Path(int Support, int Cluster)
  {
    this.Support = Support;
    this.Suffix = Cluster;
    Prefix = new List<int>();
  }
  internal Path()
  {
    Support = 0;
    Prefix = new List<int>();
  }
  internal void Add(int clusterID_in_path)
  {
    Prefix.Add(clusterID_in_path);
  }
}
#endregion Helpers
namespace FPG_cs
{
  public abstract class Abstr_FPG
  {
    #region Vars
    protected int SupplyIndex;
    public int Support
    {
      get { return SupplyIndex; }
    }
    protected Node Root;
    protected List<Element> Elements;
    protected List<int[]> Transactions;
    protected Dictionary<int, List<Node>> Element2TreeMap;
    #endregion Vars
    protected Abstr_FPG(int SI, List<Element> ConditionalElements)
    {
      SupplyIndex = SI;
      Elements = ConditionalElements;
      Transactions = new List<int[]>();
      Root = new Node(-1);
      Element2TreeMap = new Dictionary<int, List<Node>>();
    }
    protected Abstr_FPG(int Percentage)
    {
      if (Percentage > 100 || Percentage < 0)
        throw new Exception("Not a valid supply percentage!");
      SupplyIndex = Percentage;
      Elements = new List<Element>();
      Transactions = new List<int[]>();
      Root = new Node(-1);
      Element2TreeMap = new Dictionary<int, List<Node>>();
    }
    protected Abstr_FPG()
    {
      Elements = new List<Element>();
      Transactions = new List<int[]>();
      Root = new Node(-1);
      Element2TreeMap = new Dictionary<int, List<Node>>();
      SupplyIndex = 0;
    }
    #region Init
    public int GetNumberTransactions()
    {
      return Transactions.Count;
    }
    public int GetNumberOfElements()
    {
      return Elements.Count;
    }
    protected void IniMapping()
    {
      foreach (var z in Elements)
        Element2TreeMap.Add(z.ClusterId, new List<Node>());
    }
    protected void SortElementsOccurences(List<Element> Els)
    {
      bool moved = true;
      Element tmp;
      var ec = Els.Count();
      if (Els.Count() < 2)
        return;
      while (moved)
      {
        moved = false;
        for (int i = 0; i < ec; i += 2)
        {
          if (i + 1 < ec && Els[i].TotOccs < Els[i + 1].TotOccs)
          {
            moved = true;
            tmp = Els[i];
            Els[i] = Els[i + 1];
            Els[i + 1] = tmp;
          }
        }
        for (int i = 1; i < Els.Count(); i += 2)
        {

          if (i + 1 < ec && Els[i].TotOccs < Els[i + 1].TotOccs)
          {
            moved = true;
            tmp = Els[i];
            Els[i] = Els[i + 1];
            Els[i + 1] = tmp;
          }
        }
      }
    }
    protected void SortTransactionsElementsOccurences()
    {
      var SortedTransactions = new List<int[]>();
      foreach (var transaction in Transactions)
      {
        List<int> SortedOne = new List<int>();
        foreach (var sortedElement in Elements)
        {
          if (transaction.Contains(sortedElement.ClusterId))
            SortedOne.Add(sortedElement.ClusterId);
        }
        SortedTransactions.Add(SortedOne.ToArray());
      }
      Transactions.Clear();
      Transactions = SortedTransactions;
    }
    protected void AddSortedTransaction(int[] t, int pos, Node e)
    {
      if (pos == t.Count())
        return;
      else
      {
        if (!e.childs.Exists(x => x.Cluster_id == t[pos]))
        {
          Node newOne = new Node(t[pos]);
          newOne.Anchestor = e;
          e.childs.Add(newOne);
          Element2TreeMap[newOne.Cluster_id].Add(newOne);
          AddSortedTransaction(t, ++pos, newOne);
        }
        else
        {
          e.childs.Single(x => x.Cluster_id == t[pos]).Occurences++;
          AddSortedTransaction(t, ++pos, e.childs.Single(x => x.Cluster_id == t[pos - 1]));
        }
      }
    }
    public void BuildTree()
    {
      SortElementsOccurences(Elements);
      SortTransactionsElementsOccurences();
      IniMapping();
      Transactions.ForEach(e => AddSortedTransaction(e, 0, Root));
      if (SupplyIndex > 0)
        SupplyIndex = Convert.ToInt32(Math.Ceiling(((float)SupplyIndex / 100) * Transactions.Count));
    }
    #endregion Init
  }

  public class FPG_Tree : Abstr_FPG
  {
    public class Itemset
    {
      public int Ancedent;
      public float Support;
      public string SuppCount;
      public float Confidence;
      public float Lift;
      public int[] Consequence;
      public string StrConsequence;
      public int ConsequenceOccurence;
      public float Leverege;
      public float ConsequenceRarity;
      public Itemset(Path p, FPG_Tree t)
      {
        Support = ((float)p.Support) / t.Transactions.Count();
        SuppCount = p.Support.ToString() + "/" + t.Transactions.Count().ToString() + " = " + Math.Round((float)p.Support * 100 / t.Transactions.Count(), 2).ToString() + " %";
        Confidence = ((float)p.Support) / t.Elements.Single(x => x.ClusterId == p.Suffix).TotOccs;
        Ancedent = p.Suffix;
        Consequence = p.Prefix.ToArray();
        p.Prefix.ForEach(x => StrConsequence += x.ToString() + ",");
        StrConsequence = StrConsequence.Trim(',');
        ConsequenceRarity = 1.0f;
        if (!ConsequenceDB.TryGetValue(p.Prefix.ToArray(), out ConsequenceOccurence)) // если не нашли в "словаре последствий", то ищем по транзакциям консеквент
        {
          ConsequenceOccurence = 0;
          foreach (var SingleTransaction in t.Transactions)
          {
            ConsequenceOccurence++;
            foreach (var i in p.Prefix)
            {
              if (!SingleTransaction.Contains(i))
              {
                ConsequenceOccurence--;
                break;
              }
            }
          }
          ConsequenceDB.Add(p.Prefix.ToArray(), ConsequenceOccurence);
        }
        Lift = ((float)(t.Transactions.Count * p.Support)) / (t.Elements.Single(x => x.ClusterId == Ancedent).TotOccs * ConsequenceOccurence);
        Leverege = Support - (((float)t.Elements.Single(x => x.ClusterId == p.Suffix).TotOccs) / t.Transactions.Count) * (((float)ConsequenceOccurence) / t.Transactions.Count);
        foreach (var cl in Consequence)
          ConsequenceRarity *= Convert.ToSingle(Math.Exp((float)t.Transactions.Count / (t.Elements.Single(x => x.ClusterId == cl).TotOccs)));
      }
    }
    #region Vars
    private static Dictionary<int, List<Path>> AllLongestSatisfiedPaths2Nodes;
    protected int ConfidenceTreshold;
    public int Conf
    {
      get { return ConfidenceTreshold; }
    }
    private static Dictionary<int[], int> ConsequenceDB;
    public static List<Itemset> Itemsets;
    #endregion Vars
    #region Init
    public void AddTransactionsDB(List<int[]> iniDB)
    {
      iniDB.ForEach(x => AddSet(x));
    }
    public void AddSet(int[] Transaction)
    {
      Transactions.Add(Transaction);
      foreach (var z in Transaction)
      {
        if (Elements.Exists(e => e.ClusterId == z))
          Elements.Single(e => e.ClusterId == z).TotOccs++;
        else
          Elements.Add(new Element(z));
      }
    }
    #endregion Init
    public FPG_Tree(int SI = 0) : base(SI)
    {
      AllLongestSatisfiedPaths2Nodes = new Dictionary<int, List<Path>>();
      ConsequenceDB = new Dictionary<int[], int>();
      Itemsets = new List<Itemset>();
    }
    public FPG_Tree()
    {
      AllLongestSatisfiedPaths2Nodes = new Dictionary<int, List<Path>>();
      ConsequenceDB = new Dictionary<int[], int>();
      Itemsets = new List<Itemset>();
    }
    #region Methods
    public void PerformFPGAnalysis(int SI = 0, int Confidence = 0, bool FilterByLift = false)
    {
      if (SupplyIndex == 0 && SI > 0)
        SupplyIndex = Convert.ToInt32(Math.Ceiling(((float)SI / 100) * Transactions.Count));
      else
        throw new Exception("Specify support measure to perform analysis");

      foreach (var e in Elements.Where(x => x.TotOccs >= SupplyIndex))
      {
        AllLongestSatisfiedPaths2Nodes.Add(e.ClusterId, new List<Path>());
        MakePath(Element2TreeMap[e.ClusterId]);
      }
      foreach (var item in AllLongestSatisfiedPaths2Nodes)
        foreach (var path in item.Value)
          Itemsets.Add(new Itemset(path, this));
      if (Confidence > 0)
      {
        Itemsets.RemoveAll(x => x.Confidence < (float)Confidence / 100);
        ConfidenceTreshold = Confidence;
      }
      if (FilterByLift)
        Itemsets.RemoveAll(x => x.Lift < 1);

    }
    private void MakePath(List<Node> ndz)
    {
      var Paths2Bcomputed = new List<Path>();
      var Cl_Id = ndz[0].Cluster_id;
      foreach (var e in ndz)
      {
        Node n = e;
        if (n.Anchestor == Root && Element2TreeMap[Cl_Id].Count() <= 1 || Elements.Single(x => x.ClusterId == Cl_Id).TotOccs < SupplyIndex)
          return;
        else if (n.Anchestor == Root && Element2TreeMap[Cl_Id].Count() > 1)
          continue;
        var path = new Path(n.Occurences, Cl_Id);
        while (n.Anchestor != Root)
        {
          n = n.Anchestor;
          path.Add(n.Cluster_id);
        }
        Paths2Bcomputed.Add(path);
      }
      var z = ComputePaths(Paths2Bcomputed, Cl_Id);
      AllLongestSatisfiedPaths2Nodes[Cl_Id].AddRange(z);
    }
    private List<Path> ComputePaths(List<Path> AllPaths, int CalculatedCluster)
    {
      var consideredElements = new List<Element>();
      foreach (var currPath in AllPaths)
      {
        var currNodeSuppport = currPath.Support;
        foreach (var cluster in currPath.Prefix)
        {
          if (consideredElements.Exists(x => x.ClusterId == cluster))
            consideredElements.Find(x => x.ClusterId == cluster).TotOccs += currNodeSuppport;
          else
            consideredElements.Add(new Element(cluster, currNodeSuppport));
        }
      }
      SortElementsOccurences(consideredElements);
      ConditionalFPGTree cfpgt = new ConditionalFPGTree(SupplyIndex, consideredElements, AllPaths, CalculatedCluster);
      return cfpgt.GrowFrequentPatterns();
    }
    public List<Itemset> CalculateSequence(int[] Sequence)
    {
      var GeneratedItemsetsWithParams = new List<Itemset>();
      foreach(var clusterId in Sequence)
      {
        var SupportCount = 0;
        foreach (var t in Transactions)
          if (t.Contains(clusterId))
            SupportCount++;

        var ConsideredPath = new Path(SupportCount, clusterId);
        foreach (var RemainCluster in Sequence.Where(z => z != clusterId))
          ConsideredPath.Add(RemainCluster);
        GeneratedItemsetsWithParams.Add(new Itemset(ConsideredPath, this));
      }
      return GeneratedItemsetsWithParams;
    }

    public void Initialize()
    {
      var ListIniTransactions = new List<int[]>();
      try
      {
        string ConnectionString = "User ID=sysdba;Password=masterkey;" +
        "Database=localhost:C:\\Users\\4igur\\Documents\\Toyota_DBs\\ToyotaDB.fdb; " +
        "DataSource=localhost;Charset=WIN1251;";
        var CON = new FbConnection(ConnectionString);
        CON.Open();
        var TR = CON.BeginTransaction();
        var fbcCommand = new FbCommand(@" SELECT /*DIST_LIST_CLUST_IDS*/ DIST_SECOND_LEVEL_LIST_CLUSTERS FROM LIST_FACTORY_REQUESTS", CON, TR);
        FbDataReader reader2 = fbcCommand.ExecuteReader();
        while (reader2.Read())
        {
          string[] Distinct_List_Of_Delivered_Clusters_2_Factory_During_A_SingleDay = reader2[0].ToString().Split(',').ToArray();
          int[] SingleDay = new int[Distinct_List_Of_Delivered_Clusters_2_Factory_During_A_SingleDay.Count()];
          for (int i = SingleDay.Length - 1; i >= 0; i--)
            SingleDay[i] = int.Parse(Distinct_List_Of_Delivered_Clusters_2_Factory_During_A_SingleDay[i]);
          ListIniTransactions.Add(SingleDay);
        }
        TR.Commit();
        reader2.Close();
        CON.Close();
        fbcCommand.Dispose();
        AddTransactionsDB(ListIniTransactions);
        BuildTree();
      }
      catch (Exception x)
      {
        Console.WriteLine("\nShet happened in Initialize()\n");
      }
    }

    #endregion Methods
  }

  class ConditionalFPGTree : Abstr_FPG
  {
    List<Path> FrequentPaths;
    internal int CondidtionalElement;
    List<List<Element>> AllFPGs;
    internal ConditionalFPGTree(int SI, List<Element> satisfiedElements, List<Path> AllPaths, int CalculatedCluster) : base(SI, satisfiedElements)
    {
      CondidtionalElement = CalculatedCluster;
      Element2TreeMap = new Dictionary<int, List<Node>>();
      FrequentPaths = new List<Path>();
      AllFPGs = new List<List<Element>>();
      ConvertPathsToTransactions(AllPaths);
      BuildConditionalTree();
    }
    private void BuildConditionalTree()
    {
      IniMapping();
      Transactions.ForEach(e => AddConditionalTransaction(e, e.Length - 1, Root));
    }
    protected void AddConditionalTransaction(int[] t, int pos, Node e)
    {
      if (pos == -1)
        return;
      else
      {
        if (!e.childs.Exists(x => x.Cluster_id == t[pos]))
        {
          Node newOne = new Node(t[pos]);
          newOne.Anchestor = e;
          e.childs.Add(newOne);
          Element2TreeMap[newOne.Cluster_id].Add(newOne);
          AddConditionalTransaction(t, --pos, newOne);
        }
        else
        {
          e.childs.Single(x => x.Cluster_id == t[pos]).Occurences++;
          AddConditionalTransaction(t, --pos, e.childs.Single(x => x.Cluster_id == t[pos + 1]));
        }
      }
    }
    internal void ConvertPathsToTransactions(List<Path> AllPaths)
    {
      foreach (var z in AllPaths)
        for (int i = 0; i < z.Support; i++)         //<<== !!
          Transactions.Add(z.Prefix.ToArray());     //<<== !!
    }
    internal List<Path> GrowFrequentPatterns()
    {
      foreach (var e in Elements.Where(x => x.TotOccs >= SupplyIndex && x.ClusterId != CondidtionalElement))
      {
        FrequentPaths.Add(new Path(0, 0));
        var newPattern = new List<Element>();
        foreach (var n in Element2TreeMap[e.ClusterId])
          RecursivePathWalker(n, newPattern);
        if (newPattern.Exists(x => x.TotOccs >= SupplyIndex))
        {
          FrequentPaths.Last().Support = newPattern.Where(x => x.TotOccs >= SupplyIndex).Min(x => x.TotOccs);
          FrequentPaths.Last().Prefix.AddRange(newPattern.Where(x => x.TotOccs >= SupplyIndex).Select(x => x.ClusterId).ToList());
          FrequentPaths.Last().Suffix = CondidtionalElement;
        }
        else
          FrequentPaths.Remove(FrequentPaths.Last());
      }
      return FrequentPaths;
    }
    internal void RecursivePathWalker(Node n, List<Element> Pattern)
    {
      if (n.Cluster_id == CondidtionalElement)
        return;
      if (Pattern.Exists(x => x.ClusterId == n.Cluster_id))
        Pattern.Single(x => x.ClusterId == n.Cluster_id).TotOccs += n.Occurences;
      else
        Pattern.Add(new Element(n.Cluster_id, n.Occurences));
      foreach (var ndz in n.childs.Where(x => x.Cluster_id != CondidtionalElement))
        RecursivePathWalker(ndz, Pattern);
    }
  }
}